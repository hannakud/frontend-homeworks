// Practice

// Task#1 Підрахунок суми цифр числа;  ------  Однакова задача з попередньої теми

const submitTask1 = document.getElementById("submit-task1");
const numTask1 = document.getElementById("num-task1");
const resultTask1 = document.getElementById("result-task1");

submitTask1.addEventListener("click", function () {
  const number = Number(numTask1.value);
  getSum(number);
});

function getSum(number) {
  let numberArray = String(number).split("");
  let sum = numberArray.reduce((acc, digit) => acc + Number(digit), 0);

  resultTask1.innerText = sum;
}
// Task#2 Виведення цифр числа справа наліво

const submitTask2 = document.getElementById("submit-task2");
const numTask2 = document.getElementById("num-task2");
const resultTask2 = document.getElementById("result-task2");

submitTask2.addEventListener("click", function () {
  const number = numTask2.value;
  reverseNumber(number);
});

function reverseNumber(number) {
  return (resultTask2.innerText = number.split("").reverse().join(""));
}

// Task#3 Підрахунок кількості входжень заданого символу в рядку

const submitTask3 = document.getElementById("submit-task3");
const input1Task3 = document.getElementById("input1-task3");
const input2Task3 = document.getElementById("input2-task3");
const resultTask3 = document.getElementById("result-task3");

submitTask3.addEventListener("click", function () {
  const string = input1Task3.value;
  const symbol = input2Task3.value;
  calcSymbInString(string, symbol);
});

function calcSymbInString(string, symbol) {
  let count = 0;
  for (i = 0; i < string.length; i++) {
    if (string[i] === symbol) {
      count++;
    }
  }
  resultTask3.innerText = count;
}
