// Практика, завдання 3 від 24.01.2023 Фіксація меню при прокручуванні.

const header = document.querySelector("#header");
// Створюємо функцію для фіксації хедеру за допомоою властивості інтерфейсу window.scrollY
function placeMenu() {
  header.style.top = window.scrollY + "px";
}
// додаємо обробника подій scroll
document.addEventListener("scroll", placeMenu);
