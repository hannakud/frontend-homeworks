// task#1 Користувач вводить 10 чисел. Знайти максимальне число.

const submitTask1 = document.getElementById("submit-task1");
const numTask1 = document.getElementById("num-task1");
const resultTask1 = document.getElementById("result-task1");

submitTask1.addEventListener("click", function () {
  const numbers = numTask1.value.split(",").map((el) => Number(el.trim()));
  getMaxNumber(numbers);
});

function getMaxNumber(numbers) {
  let max = numbers[0];
  numbers.forEach(function (el) {
    if (el > max) max = el;
  });
  return (resultTask1.innerText = `Max number is ${max}`);
}

// task#2 Спортсмен пробігає за 1-й день М км, кожного наступного дня він збільшує норму пробігу на К%. Визначте через скільки днів норма пробігу може стати більше 50 км

const submitTask2 = document.getElementById("submit-task2");
const days = document.getElementById("days");
const percent = document.getElementById("percent");
const resultTask2 = document.getElementById("result-task2");

submitTask2.addEventListener("click", function () {
  const m = Number(days.value);
  const k = Number(percent.value);
  daysCount(m, k);
});

function daysCount(m, k) {
  let days = 0;
  while (m <= 50) {
    m += (m * k) / 100;
    days++;
  }
  resultTask2.innerText = `After ${days} days mileage rate will be more than 50km`;
}
