// Необхідно реалізувати форму додавання реєстрації кандидата на співбесіду. Форма має містити поле для введення скілу користувача. Поруч із цим полем має бути знак "+" після натискання на який з'являється ще одне поле для введення скілу.

$(function () {
  $(".add-skill").click(function (event) {
    event.preventDefault();
    $(".form-row").append(
      ' <input type="text" class="form-control add-field" id="input-skills"/>'
    );
  });
  $("#form").submit(function (event) {
    event.preventDefault();
    let allInputsFilled = true;
    $("input").each(function () {
      if ($(this).val().length === 0) {
        allInputsFilled = false;
        return false;
      }
    });
    if (!allInputsFilled) {
      alertify.error("Please, fill all fields");
    } else {
      alertify.success("Registration is submitted!");
    }
  });
});
