// task #1 Визначити, чи є число непарним.
const submit1 = document.getElementById("submit1");
const input1 = document.getElementById("input1");
const result1 = document.getElementById("result1");

submit1.addEventListener("click", function () {
  const number = Number(input1.value);
  checkNumber(number);
});

function checkNumber(number) {
  result1.innerText = number % 2 === 0 ? "odd" : "even";
}

// // task #2 Визначити, чи кратне задане число трьом; якщо ні, вивести залишок.
const submit2 = document.getElementById("submit2");
const input2 = document.getElementById("input2");
const result2 = document.getElementById("result2");

submit2.addEventListener("click", function () {
  const number = Number(input2.value);
  calcNumber(number);
});

function calcNumber(num) {
  result2.innerText = num % 3 ? num % 3 : true;
}

// task #3 Вивести всі можливі варіанти тризначного числа.
const submit3 = document.getElementById("submit3");
const input3 = document.getElementById("input3");
const result3 = document.getElementById("result3");

submit3.addEventListener("click", function () {
  const number = Number(input3.value);
  getAllNumbers(number);
});

function getAllNumbers(number) {
  // преобразуем число в строку
  number = number.toString();
  const result = [];
  // перебираем все возможные комбинации цифр
  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      for (let k = 0; k < 3; k++) {
        // проверяем, что индексы i, j, k уникальны
        if (i !== j && i !== k && j !== k) {
          // создаем число из трех цифр и добавляем его в результирующий массив
          const num = Number(number[i] + number[j] + number[k]);
          if (!result.includes(num)) {
            result.push(num);
          }
        }
      }
    }
  }
  result3.innerText = result.join("  ");
}
