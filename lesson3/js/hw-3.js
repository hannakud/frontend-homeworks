// Task#1 необхідно реалізувати функцію, що друкує всі прості числа із заданого діапазону.

const submitTask1 = document.getElementById("submit-task1");
const num1Task1 = document.getElementById("num1-task1");
const num2Task1 = document.getElementById("num2-task1");
const resultTask1 = document.getElementById("result-task1");

submitTask1.addEventListener("click", function () {
  const n1 = Number(num1Task1.value);
  const n2 = Number(num2Task1.value);
  getPrimes(n1, n2);
});

function isPrime(n) {
  if (n === 1 || n === 0) {
    return true;
  } else {
    for (let i = 2; i < n; i++) {
      if (n % i === 0) {
        return false;
      }
    }
    return true;
  }
}

function getPrimes(n1, n2) {
  let primes = [];
  for (let i = n1; i < n2; i += 1) {
    if (isPrime(i)) {
      primes.push(i);
    }
  }
  console.log(primes);
  resultTask1.innerText = primes.join("  ");
}

// Task#2 Знайти суму цифр заданого числа.

const submitTask2 = document.getElementById("submit-task2");
const numTask2 = document.getElementById("num-task2");
const resultTask2 = document.getElementById("result-task2");

submitTask2.addEventListener("click", function () {
  const number = Number(numTask2.value);
  getSum(number);
});

function getSum(number) {
  let numberArray = String(number).split("");
  let sum = numberArray.reduce((acc, digit) => acc + Number(digit), 0);

  resultTask2.innerText = sum;
}

// task #3 Дано натуральні числа n і m. Написати функцію, яка повертає результат операції додавання двох чисел. Перше утворено з k молодших цифр числа n, друге - з k старших цифр числа m.

const submitTask3 = document.getElementById("submit-task3");
const num1Task3 = document.getElementById("num1-task3");
const num2Task3 = document.getElementById("num2-task3");
const resultTask3 = document.getElementById("result-task3");

submitTask3.addEventListener("click", function () {
  const n = Number(num1Task3.value);
  const m = Number(num2Task3.value);
  concatDigsts(n, m, 2);
});

function concatDigsts(n, m, k) {
  let mString = String(n);
  let end = String(m).substring(0, k);
  let start = mString.substring(mString.length, mString.length - k);
  return (resultTask3.innerText = Number(start + end));
}
