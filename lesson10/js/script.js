// setTimeout(() => {
//   $.get("ajax.php", (data) => {
//     $(".root").html(data);
//     console.log(data);
//   });
// }, 1000);
let number = 0;

function getNews(num) {
  $.get("ajax.php", { news_count: num }, (data) => {
    $(".root").html(data);
    console.log(data);
  });
}

getNews(number);

$(".next").click(() => {
  number++;
  getNews(number);
});

$(".prev").click(() => {
  number--;
  getNews(number);
});
