let slides = document.querySelectorAll(".slide");
let pagination = document.getElementById("pagination");
const prevBtn = document.getElementById("prevBtn");
const nextBtn = document.getElementById("nextBtn");

// виконуємо цикл для додавання кнопок пагінації до слайдеру
for (let i = 0; i < slides.length; i++) {
  const button = document.createElement("button");
  button.classList.add("page");
  button.innerText = "o";
  pagination.appendChild(button);
}

let pages = document.querySelectorAll(".page");

// номер активного слайду в масивы слайдів
let currentSlide = 0;

// додаємо класс до активного сладу
slides[currentSlide].classList.add("active");

// обробка кліку на наступний слайд
function nextSlide() {
  if (currentSlide >= slides.length - 1) return;
  slides[currentSlide].classList.remove("active");
  currentSlide++;
  slides[currentSlide].classList.add("active");
}

// обробка кліку на попередній слайд
function prevSlide() {
  if (currentSlide <= 0) return;
  slides[currentSlide].classList.remove("active");
  currentSlide--;
  slides[currentSlide].classList.add("active");
}

// створюємо функцію  переходу на конкретний слайд
function setSlide(i) {
  slides[currentSlide].classList.remove("active");
  currentSlide = i;
  slides[currentSlide].classList.add("active");
}
// в циклі додаємо обробника подій
for (let i = 0; i < slides.length; i++) {
  pages[i].addEventListener("click", () => setSlide(i));
}

// додаємо обробник події на клік в слайдері
prevBtn.addEventListener("click", prevSlide);
nextBtn.addEventListener("click", nextSlide);
