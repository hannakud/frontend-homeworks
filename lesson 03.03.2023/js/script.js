const ref = {
  newsNav: $("#news"),
  addNav: $("#add-news"),
  listSection: $(".list"),
  detailsSection: $(".details"),
  addSection: $(".add-news-form "),
  title: $("#title"),
  body: $("#text"),
};

function loadNewsList() {
  ref.listSection.html("");
  ref.listSection.addClass("loading");
  $.getJSON("/api/news/list/")
    .done(({ list }) => {
      list.forEach((title, index) => {
        ref.listSection.append(`
				<li>
					<span>${title}</span>
					<button class="btn-view btn btn-secondary" data-id="${index}">view</button>
				</li>
				`);
      });
    })
    .fail((error) => {
      ref.listSection.html("Opps");
    })
    .always(() => {
      setTimeout(() => {
        ref.listSection.removeClass("loading");
      }, 1000);
    });
}

function navigateToList() {
  ref.listSection.removeClass("hidden");
  ref.detailsSection.addClass("hidden");
  ref.addSection.addClass("hidden");
  ref.newsNav.addClass("active");
  ref.addNav.removeClass("active");
  loadNewsList();
}

function loadNewsDetails(id) {
  ref.detailsSection.html("");
  ref.detailsSection.addClass("loading");
  $.getJSON("/api/news/item/", { itemnumber: id })
    .done(({ title, body }) => {
      ref.detailsSection.html(`
				<h3>${title}</h3>
				<p>${body}</p>
			`);
    })
    .fail((error) => {
      ref.detailsSection.html("Opps");
    })
    .always(() => {
      setTimeout(() => {
        ref.detailsSection.removeClass("loading");
      }, 1000);
    });
}

function sendNews(title, body) {
  ref.addSection.addClass("loading");
  $.get("/api/news/add/", { posttitle: title, postdata: body })
    .done(() => {
      navigateToList();
      ref.title.val("");
      ref.body.val("");
    })
    .fail((error) => {
      console.error("sometging went wrong");
    })
    .always(() => {
      setTimeout(() => {
        ref.addSection.removeClass("loading");
      }, 1000);
    });
}

function navigateToNewsDetails(id) {
  ref.listSection.addClass("hidden");
  ref.detailsSection.removeClass("hidden");
  ref.addSection.addClass("hidden");
  ref.newsNav.removeClass("active");
  ref.addNav.removeClass("active");
  loadNewsDetails(id);
}

function navigateToForm() {
  ref.listSection.addClass("hidden");
  ref.detailsSection.addClass("hidden");
  ref.addSection.removeClass("hidden");
  ref.newsNav.removeClass("active");
  ref.addNav.addClass("active");
}

ref.newsNav.click((event) => {
  event.preventDefault();
  navigateToList();
});

ref.addNav.click((event) => {
  event.preventDefault();
  navigateToForm();
});

ref.listSection.click((event) => {
  if (event.target.nodeName === "BUTTON") {
    const id = Number(event.target.dataset.id);
    navigateToNewsDetails(id);
  }
});

ref.addSection.submit((event) => {
  event.preventDefault();

  const title = event.target[0].value;
  const body = event.target[1].value;
  sendNews(title, body);
});

navigateToList();
