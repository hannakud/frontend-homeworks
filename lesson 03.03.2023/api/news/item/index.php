<?php

class Message{
    public $errorCode;
    public $title;
    public $body;
    public function __construct($errorCode = 0, $title = "", $body = ""){
        $this->errorCode = $errorCode;
        $this->title = $title;
        $this->body = $body;
    }
}


    // 0...n
    if(isset($_GET["itemnumber"])) {

        $arr = file("../../../news.txt");

        if(sizeof($arr) - 1 < $_GET["itemnumber"] || $_GET["itemnumber"] < 0) {
            die(json_encode(new Message(1)));
        }
        $item = explode("|",$arr[$_GET["itemnumber"]]);


        die(json_encode(new Message(0, $item[0], $item[1])));
    }

    echo json_encode(new Message(1));
