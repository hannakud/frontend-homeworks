// task 1 Визначити, у які діапазони потрапляє число, введене користувачем.
const submit1 = document.getElementById("submit1");
const input1 = document.getElementById("input1");
const result1 = document.getElementById("ranges1");

submit1.addEventListener("click", function () {
  const number = Number(input1.value);
  getRange(number);
});

function getRange(number) {
  const ranges = [];
  if (number <= 13 && number >= 3) {
    ranges.push("3 to 13");
  }
  if (number <= 25 && number >= 8) {
    ranges.push("8 to 25");
  }
  if (number <= 31 && number >= 13) {
    ranges.push("13 to 31");
  }

  result1.innerText = ranges.join(" and ");
}

// task 2 Знайти більше з трьох чисел. Можна використовувати тільки один оператор if.

const submit2 = document.getElementById("submit2");
const num1 = document.getElementById("num1");
const num2 = document.getElementById("num2");
const num3 = document.getElementById("num3");
const result2 = document.getElementById("result2");

submit2.addEventListener("click", function () {
  const n1 = Number(num1.value);
  const n2 = Number(num2.value);
  const n3 = Number(num3.value);
  getMax(n1, n2, n3);
});

function getMax(n1, n2, n3) {
  let max = n1;
  if (n2 > max) {
    max = n2;
  }
  if (n3 > max) {
    max = n3;
  }
  result2.innerText = `Max number is ${max}`;
}

// task 3 Вивести всі парні числа із зазначеного діапазону
const submitTask3 = document.getElementById("submit-task3");
const num1Task3 = document.getElementById("num1-task3");
const num2Task3 = document.getElementById("num2-task3");
const resultTask3 = document.getElementById("result-task3");

submitTask3.addEventListener("click", function () {
  const number1 = Number(num1Task3.value);
  const number2 = Number(num2Task3.value);
  getEven(number1, number2);
});

function getEven(number1, number2) {
  const ranges = [];
  for (let i = number1; i <= number2; i++) {
    if (i % 2 === 0) {
      ranges.push(i);
    }
  }

  resultTask3.innerText = ranges.join("  ");
}

// task 4 Користувач вводить номер місяця, потрібно вивести пору року (літо, осінь, зима, весна)
const submitTask4 = document.getElementById("submit-task4");
const numTask4 = document.getElementById("num-task4");
const resultTask4 = document.getElementById("result-task4");

submitTask4.addEventListener("click", function () {
  const number = Number(numTask4.value);
  getSeason(number);
});

function getSeason(number) {
  switch (number) {
    case 1:
    case 2:
    case 12:
      resultTask4.innerText = "Winter";
      break;
    case 3:
    case 4:
    case 5:
      resultTask4.innerText = "Spring";
      break;
    case 6:
    case 7:
    case 8:
      resultTask4.innerText = "Summer";
      break;
    case 9:
    case 10:
    case 11:
      resultTask4.innerText = "Autumn";
      break;
  }
}
