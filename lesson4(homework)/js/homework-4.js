//  Task# 1.1 Реалізуємо функції валідації (телефон)

const submit1Task1 = document.getElementById("submit1-task1");
const tel = document.getElementById("tel");
const result1Task1 = document.getElementById("result1-task1");

submit1Task1.addEventListener("click", function () {
  const number = tel.value;
  checkValidTel(number);
});

function checkValidTel(number) {
  if (number.length === 0) {
    return (result1Task1.innerText = "Please, enter your telephone");
  }
  if (!number.startsWith("+38")) {
    return (result1Task1.innerText =
      "If you are from Ukraine, you should start from +380..");
  }
  if (!number.startsWith("+")) {
    return (result1Task1.innerText = "Number should start with '+'");
  }

  if (number.length > 13) {
    return (result1Task1.innerText = "To many digits");
  }
  if (number.length < 13) {
    return (result1Task1.innerText = "Not enough digits");
  }

  return (result1Task1.innerText = `${number} is valid telephone number`);
}

//  Task# 1.2 Реалізуємо функції валідації (email);

const submit2Task1 = document.getElementById("submit2-task1");
const mail = document.getElementById("mail");
const result2Task1 = document.getElementById("result2-task1");

submit2Task1.addEventListener("click", function () {
  const email = mail.value;
  checkValidEmail(email);
});
function checkValidEmail(email) {
  // валідація англійського алфавіту
  const correctValue = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@.";
  for (let i = 0; i < email.length; i++) {
    if (!correctValue.includes(email[i])) {
      return (result2Task1.innerText = `${email} is not valid email, enter only english letters`);
    }
  }

  // валідація @ у введенному email
  if (!email.includes("@")) {
    return (result2Task1.innerText = `${email} is not valid email, email should contain @`);
  }

  // валідація довжини домену email
  if (email.split("@")[1].split(".")[1].length <= 2) {
    return (result2Task1.innerText = `${email} is not valid email, domain should be at least 2 letters long`);
  }

  // валідація довжини тексту після @(більше 1, тому що буває пошта index@i.ua)
  if (email.split("@")[1].split(".")[0].length <= 1) {
    return (result2Task1.innerText = `${email} is not valid email, after @ you should enter at least 1 letter`);
  }

  return (result2Task1.innerText = `${email} is correct`);
}

//  Task# 1.3 Реалізуємо функції валідації (ПІБ);
const submit3Task1 = document.getElementById("submit3-task1");
const text = document.getElementById("name");
const result3Task1 = document.getElementById("result3-task1");

submit3Task1.addEventListener("click", function () {
  const pip = text.value;
  isValid(pip);
});

function isValid(pip) {
  //   перевірити, що нема спец. символів та цифр у строці (без регулярних виразів)
  const forbiddenSymbols = "0123456789!#$%&'()*+,-./:;\"<=>?@[]^_`{|}~";
  for (let i = 0; i < pip.length; i++) {
    if (forbiddenSymbols.includes(pip[i])) {
      return (result3Task1.innerText = "Numbers ans symbols are forbidden");
    }
  }
  // перевірити кількість пробілів у строці
  if (pip.split(" ").length - 1 < 2) {
    return (result3Task1.innerText = `You should enter fullname(Surname, Name, Fathers's name)`);
  }
  return (result3Task1.innerText = `${pip} is correct fullname`);
}

//Taks #2 Реалізуємо функцію підрахунку грошей, що видаються банкоматом. Користувач запитує суму, а банкомат має підібрати варіант із найбільшими купюрами. Врахувати, що кількість банкнот обмежена.

function createAtm() {
  // Створюємо набір доступних купюр
  let cash = {
    20: 100,
    50: 100,
    100: 100,
    200: 100,
    500: 100,
    1000: 100,
  };
  const banknotesList = Object.keys(cash)
    .map((el) => Number(el))
    .sort((a, b) => b - a);

  // Функція для повернення готівки в банкомат із буфера, якшо транзакція не можлива
  function resetTransaction(banknotes) {
    banknotesList.forEach((banknote) => {
      cash[String(banknote)] += banknotes[String(banknote)];
    });
  }

  // Функція видачі готівки
  function withdrawal(amount, banknotes = {}) {
    console.log(amount, banknotes);
    // Формуємо порожній набір банкнот
    if (Object.keys(banknotes).length === 0) {
      banknotesList.forEach((banknote) => {
        banknotes[String(banknote)] = 0;
      });
    }
    // Повертаємо набір банкнот коли сума для видачі сформована
    if (amount === 0) {
      return banknotes;
    }

    for (i = 0; i < banknotesList.length; i++) {
      const banknote = banknotesList[i];
      const banknoteName = String(banknote);
      // Знаходиму найбільшу купюру в банкоматі меньшу за решту суми
      if (amount >= banknote && cash[banknoteName] > 0) {
        // Вираховуєму купюру з решти суми
        const restAmount = amount - banknote;
        // Додаємо купюру до набору банкнот
        banknotes[banknoteName] += 1;
        // Вираховуєму купюру з доступних в банкоматі
        cash[banknoteName] -= 1;
        // Рекурсино викликаємо функцію видачі з рештою і набором купюр
        return withdrawal(restAmount, banknotes);
      }
    }
    // Повертаємо помилку коли недотатньо коштів в банкоматі
    if (banknotesList.every((banknote) => !cash[String(banknote)])) {
      resetTransaction(banknotes);
      return "error: Not enough cash in atm";
    }
    // Повертаємо помилку коли решта суми меньша за наймейншу купюру
    if (banknotesList.every((banknote) => banknote > amount)) {
      resetTransaction(banknotes);
      return "Помилка: неможливо видати сумму доступними купюрами";
    }
  }

  // Повертаємо функцію видачі готівки із замикання. Далі зможемо додавати інші функції
  return {
    withdrawal,
  };
}

const atm = createAtm();
console.log(atm.withdrawal(1210));
console.log(atm.withdrawal(1450));
console.log(atm.withdrawal(5000));
