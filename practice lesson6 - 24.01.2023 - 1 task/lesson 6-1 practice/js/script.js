// Практика, завдання 1 від 24.01.2023

// При натисканні на кнопку відправлення форми необхідно провести валідацію таких полів:
// - Email не порожній
// - Пароль і підтвердження не порожні та збігаються
// - Поле ПІБ містять три елементи.

const form = document.getElementById("form");
const submitButton = document.getElementById("submit");
const email = document.getElementById("email");
const password = document.getElementById("password");
const confirmPassword = document.getElementById("confirm-password");
const fullname = document.getElementById("name");

// Додаємо addEventListener до форми, щоб він відслідковував подію "submit":
form.addEventListener("submit", (event) => {
  // Перевіряємо, чи email не порожній
  if (email.value === "") {
    alert("Email не може бути порожнім");
    event.preventDefault();
  }
  // Перевіряємо, чи пароль та підтвердження не порожні

  if (password.value === "" || confirmPassword.value === "") {
    alert("Пароль та підтвердження не можуть бути порожніми");
    // щоб зупинити подальшу відправку форми, якщо валідація не пройшла успішно, додаємо preventDefault();
    event.preventDefault();
  }
  //
  if (password.value !== confirmPassword.value) {
    alert("Пароль та підтвердження не збігаються");
    event.preventDefault();
  }

  // Перевіряємо, що поле ПІБ містять три елементи.

  if (fullname.value.split(" ").length < 3) {
    alert("Please insert full name in format: Бандера Степан Андрійович");
    event.preventDefault();
  }
});
