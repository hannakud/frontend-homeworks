$(() => {
  $("#formSubmit").click(addCategory);
  showCategoriesTable();
  showUserContent();
  $("#editFormSubmit").click(applyEditCategory);
});
const categories = {
  list: [
    {
      ID: 1,
      parentId: 0,
      title: "JS",
      description: "some desc",
      image:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/480px-Unofficial_JavaScript_logo_2.svg.png",
    },
    {
      ID: 2,
      parentId: 0,
      title: "PHP",
      description: "some desc",
      image:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/297px-PHP-logo.svg.png",
    },
    {
      ID: 3,
      parentId: 0,
      title: "C++",
      description: "some desc",
      image:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/240px-ISO_C%2B%2B_Logo.svg.png",
    },
    {
      ID: 4,
      parentId: 1,
      title: "ReactJS",
      description: "some desc",
      image:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/480px-React-icon.svg.png",
    },
  ],
};
localStorage.setItem("categories", JSON.stringify(categories));

function showCategoriesTable() {
  let list = localStorage.getItem("categories");
  list = JSON.parse(list);

  $("#categoriesList").html("");
  list.list.map((item) => {
    $("#categoriesList").append(
      "<tr>" +
        "<td>" +
        item.ID +
        "</td>" +
        "<td><img src='" +
        item.image +
        "'></td>" +
        "<td>" +
        getCategoryTitle(item.parentId) +
        "</td>" +
        "<td>" +
        item.title +
        "</td>" +
        "<td><a href='#' onclick='editCategory(" +
        item.ID +
        ")'>Edit</a></td>" +
        "<td><a href='#' onclick='deleteCategory(" +
        item.ID +
        ")'>Delete</a></td>" +
        "<tr>"
    );
  });
}

function getCategoryTitle(id) {
  let list = localStorage.getItem("categories");
  list = JSON.parse(list);

  let category = list.list.find((item) => {
    return item.ID == id;
  });

  return category === undefined ? "" : category.title;
}

function editCategory(id) {
  category = categories.list.find((el) => el.ID === id);
  $("#editId").val(id);
  $("#editTitle").val(category.title);
  $("#editParentCat").val(category.parentId);
  $("#editDescription").val(category.description);
  $("#editImage").val(category.image);
  $("#editModal").removeClass("hidden");
}

$("#closeModalBtn").click(() => {
  $("#editModal").addClass("hidden");
});

function deleteCategory(id) {
  let categories = localStorage.getItem("categories");
  categories = JSON.parse(categories);

  categories = categories.list.filter((item) => {
    return item.ID != id;
  });

  let str = '{"list":' + JSON.stringify(categories) + "}";
  localStorage.setItem("categories", str);

  showCategoriesTable();
  showUserContent();
}

function addCategory() {
  let title = $("#title").val();
  let parentCat = $("#parentCat").val();
  let description = $("#description").val();
  let image = $("#image").val();

  let categories = localStorage.getItem("categories");
  categories = JSON.parse(categories);

  let nextID =
    Math.max(
      ...categories.list.map((item) => {
        return item.ID;
      })
    ) + 1;
  console.log(nextID);

  categories.list.push({
    ID: nextID,
    parentId: parentCat,
    title: title,
    description: description,
    image: image,
  });

  localStorage.setItem("categories", JSON.stringify(categories));

  showCategoriesTable();
  showUserContent();
}

function applyEditCategory() {
  const id = $("#editId").val();

  const title = $("#editTitle").val();
  const parentCat = $("#editParentCat").val();
  const desk = $("#editDescription").val();
  const img = $("#editImage").val();

  category = categories.list.find((el) => String(el.ID) === String(id));

  category.title = title;
  category.parentId = parentCat;
  category.description = desk;
  image.parentId = img;

  localStorage.setItem("categories", JSON.stringify(categories));

  showCategoriesTable();
  showUserContent();
  $("#editModal").addClass("hidden");
}

function showUserContent() {
  let list = localStorage.getItem("categories");
  list = JSON.parse(list);

  $("#tab2-wrapper").html("");
  list.list.map((item) => {
    $("#tab2-wrapper").append(
      "<ul><li><img src='" +
        item.image +
        "'><p class='img-title'>" +
        item.title +
        "</p></li></ul>"
    );
  });
}
showUserContent();
const $tabs = $(".tab-container button, .tab-container a");
const $tabContents = $(".tab-content-container div");

$tabs.on("click", function (event) {
  event.preventDefault();
  $tabs.removeClass("active");
  $tabContents.removeClass("active");

  $(this).addClass("active");
  const $tabContent = $(`#${$(this).data("target")}`);
  if ($tabContent.length) {
    $tabContent.addClass("active");
  }
});
