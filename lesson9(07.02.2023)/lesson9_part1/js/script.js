// Створюємо перемикач із плавним перемиканням і зміною кольору

$(".button").click(function () {
  $(this).toggleClass("switch-on");
  if ($(this).hasClass("switch-on")) {
    $(".title").removeClass("title-off").addClass("title-on");
    $(this).trigger("on.switch");
  } else {
    $(".title").removeClass("title-on").addClass("title-off");
    $(this).trigger("off.switch");
  }
});
