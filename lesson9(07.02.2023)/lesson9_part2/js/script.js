// Мультикрокова форма реєстрації користувача.
// Підсвічування кроку має плавно переміщатися від елемента до елемента.

let step = 1;
function renderProgressBar() {
  const progressSteps = $(".progress-step");
  progressSteps.each(function () {
    if ($(this).data("progress-step") <= step) {
      $(this).addClass("progress-step-active");
    } else {
      $(this).removeClass("progress-step-active");
    }
  });
  $(".progress-line")[0].dataset.step = step;
}

function renderInputs() {
  const progressSteps = $(".form-row");
  progressSteps.each(function () {
    if ($(this).data("form-step") === step) {
      $(this).removeClass("hidden");
    } else {
      $(this).addClass("hidden");
    }
  });
}

function renderButtons() {
  const submit = $(".btn-submit");
  const next = $(".btn-next");
  const prev = $(".btn-prev");

  if (step === 1) {
    submit.addClass("hidden");
    next.removeClass("hidden");
    prev.addClass("hidden");
  } else if (step === 4) {
    submit.removeClass("hidden");
    next.addClass("hidden");
    prev.removeClass("hidden");
  } else {
    submit.addClass("hidden");
    next.removeClass("hidden");
    prev.removeClass("hidden");
  }
}
function render() {
  renderProgressBar();
  renderInputs();
  renderButtons();
}

function handleNext() {
  step += 1;
  render();
}

function handlePrev() {
  step -= 1;
  render();
}

function setStep(event) {
  const targetStep = Number(event.target.dataset.progressStep);
  if (step > targetStep) {
    step = targetStep;
    render();
  }
}

render();

$(".btn-next").click(handleNext);
$(".btn-prev").click(handlePrev);
$(".progress-step").click(setStep);

$("#form").submit(function (event) {
  event.preventDefault();
  validateForm();
});

function validateForm() {
  let allInputsFilled = true;
  $("input").each(function () {
    if ($(this).val().length === 0) {
      allInputsFilled = false;
      return false;
    }
  });
  if (!allInputsFilled) {
    alertify.error("Please, fill all fields in the form");
  } else {
    alertify.success("Registration is submitted!");
  }
}
